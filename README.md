# optimizetic-pack

Some nice Minecraft fabric mods, all presented for you. (1.17.1)  

Most mods are optimizers, and make your game run better, though there are quality of life improvements included.  

I take credit for none other than dynSFX, which I only modified, not created.  
All these mods are licensed as: All Rights Reserved (and posted on GitHub), licensed as MIT, GPL, MPL, or Apache.  
All mods have not been renamed and can be found on one of the following platforms, under their respective names:
- Gitlab
- Github
- Bitbucket
- Modrinth
- Curseforge

---

Most mods interface with modMenu, and their internet listings can be found there as well.  

`disabled/`: mods that I personally have disabled, but you may find them useful.  

---

No promises on updates, but fabrilous updater should do most of the work for you.  
I do not condone nor partake in any intellectual property theft.  
I do not condone any hacking or bans that may arise from use of these mods.  
Though I include meteor and tweakeroo, I use them mostly for its quality of life improvements, and HUD.  

---

That is all, enjoy.
